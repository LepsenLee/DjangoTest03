from django.urls import re_path, include
from booktest import views

urlpatterns = [
    re_path(r'^index$', views.index),  # 首页
    # re_path(r'^showarg(\d+)$', views.show_arg),  # 捕获url参数：位置参数
    re_path(r'^showarg(?P<num>\d+)$', views.show_arg),  # 捕获url参数：关键字参数
    re_path(r'^login$', views.login),  # 显示登录界面
    re_path(r'^login_check', views.login_check),  # 用户登录校验
    re_path(r'^test_ajax$', views.ajax_test),
    re_path(r'^ajax_handel$', views.ajax_handel),
    re_path(r'^login_ajax$', views.login_ajax),
    re_path(r"^login_ajax_check$", views.login_ajax_check),
]