from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse


# Create your views here.

def index(request):
    # num = 'a' + 2
    return render(request, 'booktest/index.html')


def show_arg(request, num):
    return HttpResponse(num)


def login(request):
    return render(request, 'booktest/login.html')


def login_check(request):
    """登录校验视图"""
    # request.POST 保存的是post方式提交的参数，属于QueryDict的对象
    # request.GET  保存的是 get方式提交的参数，属于QueryDict的对象
    # 1.获取提交的用户名和密码
    username = request.POST.get('username')
    password = request.POST.get('password')
    # 2.进行登录的校验
    # 实际开发：根据用户名和密码查找数据库进行比对
    if username == 'smart' and password == '123':
        # 账号和密码正确，跳转到首页
        return redirect('/index')
    else:
        # 账号和密码错误，跳转回登录
        return redirect('/login')
    # 3.返回应答
    # return HttpResponse('你好棒！')


def ajax_test(request):
    """显示Ajax页面"""
    return render(request, 'booktest/test_ajax.html')


def ajax_handel(request):
    return JsonResponse({'res': 1})


def login_ajax(request):
    """显示Ajax界面"""
    return render(request, 'booktest/login_ajax.html')


def login_ajax_check(request):
    # 1.获取提交的用户名和密码
    username = request.POST.get('username')
    password = request.POST.get('password')
    # 2.进行登录的校验，返回json数据
    if username == 'smart' and password == '123':
        # 账号和密码正确
        return JsonResponse({'res': 1})
        # return redirect('/index') 这里需要注意，Ajax的请求是在后台完成的，所以不要返回页面或者重定向
    else:
        # 账号和密码错误
        return JsonResponse({'res': 0})
